package com.example.agniwira.myapplication;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.agniwira.myapplication.adapter.ListMenuAdapter;
import com.example.agniwira.myapplication.model.ListMenuItem;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        this.setTitle("Profile");
        final TextView textView2 = (TextView) findViewById(R.id.textView2);
        final TextView textView3 = (TextView) findViewById(R.id.textView3);

        Intent intent = getIntent();
        String nameUser = intent.getStringExtra("username");
        final String haloUser = "Halo" +" "+nameUser;
        textView2.setText(R.string.hello);
        textView2.append(" ");
        textView2.append(nameUser);

        String menuString = intent.getStringExtra("menuData");
        Log.d("RESPONSE",menuString);




//        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
//        dialog.setTitle(R.string.dialog_title);
//        dialog.setMessage(textView2.getText());
//
//        dialog.setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                Toast.makeText(getApplicationContext(), "You clicked on OK", Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        dialog.setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                dialogInterface.cancel();
//            }
//        });

        //AlertDialog alert = dialog.create();
        //alert.show();

        final Dialog dialogSubmit = new Dialog(this);
        dialogSubmit.setContentView(R.layout.dialog_info);
        dialogSubmit.show();

        final EditText formInput = (EditText) dialogSubmit.findViewById(R.id.formDialog);
        Button submitButton = (Button) dialogSubmit.findViewById(R.id.submit);
        submitButton.setOnClickListener(new TextView.OnClickListener(){

            @Override
            public void onClick(View view) {
                textView3.setText(formInput.getText());
                Toast.makeText(HomeActivity.this, haloUser, Toast.LENGTH_SHORT).show();
                dialogSubmit.dismiss();
            }
        });


        //load dynamic
        final ListView listView = (ListView) findViewById(R.id.list_menu);
        ArrayList<ListMenuItem> listMenuItems = createSampleMenu(menuString);
        final ListMenuAdapter listMenuAdapter = new ListMenuAdapter(HomeActivity.this, R.layout.list_menu_layout, listMenuItems);
        listView.setAdapter(listMenuAdapter);

        // Pencet item di list view
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                String label = (listMenuAdapter.getItem(i)).getLabel()+"";
                Toast.makeText(HomeActivity.this, label , Toast.LENGTH_SHORT).show();

                //response basic auth
                textView3.setText(getIntent().getStringExtra("emailGithub").toString());


                if(label.equalsIgnoreCase("logout")){
                    getApplicationContext().startActivity(new Intent(HomeActivity.this, MainActivity.class));
                }
            }
        });







    }

    private ArrayList<ListMenuItem> createSampleMenu(String menuString){

        JSONArray jsonarray = null;

        ArrayList<ListMenuItem> listMenuJSON = new ArrayList<>();
        try {
            jsonarray = new JSONArray(menuString);
            for (int i = 0; i < jsonarray.length(); i++) {
                JSONObject jsonobject = jsonarray.getJSONObject(i);
                long id = Long.parseLong(jsonobject.getString("id"));
                String iconUrl = jsonobject.getString("iconUrl");
                String label = jsonobject.getString("Label");
                String description = jsonobject.getString("description");
                listMenuJSON.add(new ListMenuItem(id,iconUrl,label,description));
            }
            return listMenuJSON;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }
}
