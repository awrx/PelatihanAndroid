package com.example.agniwira.myapplication.service;

import com.example.agniwira.myapplication.model.Post;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by agni.wira on 14/06/17.
 */

public interface PostService {
    @GET("/posts")
    Call<List<Post>> listAll();
}
