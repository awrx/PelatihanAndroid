package com.example.agniwira.myapplication.task;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import com.example.agniwira.myapplication.R;
import com.example.agniwira.myapplication.model.Post;
import com.example.agniwira.myapplication.service.PostService;
import com.google.gson.Gson;

import org.w3c.dom.Text;

import java.io.IOException;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by agni.wira on 14/06/17.
 */

public class PostActivityTask extends AsyncTask<Object,Object,String> {
    private Context context;

    public PostActivityTask(Context ctx){
        this.context = ctx;
    }
    @Override
    protected String doInBackground(Object... objects) {

        Retrofit client = new Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PostService service = client.create(PostService.class);

        Call<List<Post>> call = service.listAll();
        Response<List<Post>> posts = null;

        try {
            posts = call.execute();
            Log.d("RESPONSE",new Gson().toJson(posts));
            //return new Gson().toJson(posts);
            return "sukses";

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;

    }

    protected void onPostExecute(String posts) {
//        if (posts!= null) {
//            Activity home = (Activity) context;
//            TextView coba = (TextView) home.findViewById(R.id.textView3);
//            //coba.setText(posts);
//        }


    }
}
