package com.example.agniwira.myapplication.service;

import com.example.agniwira.myapplication.model.ListMenuItem;
import com.example.agniwira.myapplication.model.Post;
import com.example.agniwira.myapplication.model.Repo;
import com.example.agniwira.myapplication.model.UserEmail;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by acer on 6/14/2017.
 */

public interface MenuService {
    @PUT("/v2/594200f00f0000c819c63325")
    Call<List<ListMenuItem>> listAll();

    @GET("/users/{username}/repos")
    Call<List<Repo>> listAllRepository(@Path("username") String username);

    @GET("/user/emails")
    Call<List<UserEmail>> listAllEmail(@Header("Authorization") String token);
}
