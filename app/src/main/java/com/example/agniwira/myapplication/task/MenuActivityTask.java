package com.example.agniwira.myapplication.task;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.example.agniwira.myapplication.HomeActivity;
import com.example.agniwira.myapplication.R;
import com.example.agniwira.myapplication.model.ListMenuItem;
import com.example.agniwira.myapplication.model.Post;
import com.example.agniwira.myapplication.model.Repo;
import com.example.agniwira.myapplication.model.UserEmail;
import com.example.agniwira.myapplication.service.MenuService;
import com.example.agniwira.myapplication.service.PostService;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by acer on 6/14/2017.
 */

public class MenuActivityTask extends AsyncTask<Object,Object,String> {
    private final Context context;

    public List<UserEmail> getItems() {
        return items;
    }

    private List<UserEmail> items;

    public MenuActivityTask(Context ctx) {
        this.context = ctx;
    }

    @Override
    protected void onPreExecute() {
        ((Activity) context).findViewById(R.id.progressBar).setVisibility(View.VISIBLE);

        ((Activity) context).findViewById(R.id.edittext1).setVisibility(View.INVISIBLE);
        ((Activity) context).findViewById(R.id.edittext2).setVisibility(View.INVISIBLE);
        ((Activity) context).findViewById(R.id.buttonLogin).setVisibility(View.INVISIBLE);
        ((Activity) context).findViewById(R.id.tv_login).setVisibility(View.INVISIBLE);
    }

    @Override
    protected String doInBackground(Object... objects) {

        String password = objects[0].toString();
        String username = objects[1].toString();

        String token = username + ":" + password;

        String tokenAuth = "Basic "+Base64.encodeToString(token.getBytes(),Base64.DEFAULT);

        Retrofit client = new Retrofit.Builder()
                //.baseUrl("http://www.mocky.io/")
                .baseUrl("https://api.github.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Retrofit clientMenu = new Retrofit.Builder()
                .baseUrl("http://www.mocky.io/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();



        MenuService service = clientMenu.create(MenuService.class);

        MenuService serviceEmail = client.create(MenuService.class);


        Call<List<ListMenuItem>> call = service.listAll();
        Call<List<UserEmail>> emailCall = serviceEmail.listAllEmail(tokenAuth.trim());

        Response<List<ListMenuItem>> posts = null;
        Response<List<UserEmail>> responseEmail = null;

        try {
            posts = call.execute();
            responseEmail = emailCall.execute();

            items = responseEmail.body();


            return new Gson().toJson(posts.body());


        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    protected void onPostExecute(String posts) {
        ((Activity) context).findViewById(R.id.progressBar).setVisibility(View.INVISIBLE);
        EditText etUsername= (EditText) ((Activity)context).findViewById(R.id.edittext1);

        final Intent intent = new Intent(context,HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("menuData",posts);
        intent.putExtra("username",etUsername.getText().toString());

        Log.d("RESPONSE",new Gson().toJson(items));

        intent.putExtra("emailGithub", new Gson().toJson(items));
        context.startActivity(intent);

    }
}
