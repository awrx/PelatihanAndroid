package com.example.agniwira.myapplication.model;

/**
 * Created by agni.wira on 14/06/17.
 */

public class ListMenuItem {
    private long id;
    private String iconUrl;
    private String Label;
    private String description;

    public ListMenuItem(long id, String iconUrl, String label, String description) {
        this.id = id;
        this.iconUrl = iconUrl;
        Label = label;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getLabel() {
        return Label;
    }

    public void setLabel(String label) {
        Label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
